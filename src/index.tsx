import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { RestfulProvider } from "restful-react";
import reportWebVitals from "./reportWebVitals";
import "./index.css";

ReactDOM.render(
    <React.StrictMode>
        <RestfulProvider base="http://localhost:8000">
            <App />
        </RestfulProvider>
    </React.StrictMode>,
    document.getElementById("root")
);

reportWebVitals(console.log);
