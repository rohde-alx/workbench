import {
    BrowserRouter,
    Switch,
    Route,
    Link,
    useParams,
} from "react-router-dom";
import { Task, GetTasksTasksGet, useInfoGet } from "./fetchers";
import { TreeTable, TreeState, Row } from "cp-react-tree-table";
import styles from "./App.module.css";

export default function App() {
    const { data: info } = useInfoGet({});
    return (
        <BrowserRouter>
            <div className={styles.container}>
                <header className={styles.header}>
                    <div className={styles.item}>
                        <Link to="/">Peon</Link>
                    </div>
                    <div className={styles.item}>
                        <Link to="/tasks">Tasks</Link>
                    </div>
                    <div className={styles.item}>
                        <Link to="/tasks">Resources</Link>
                    </div>
                </header>
                <Switch>
                    <Route exact path="/">
                        {info && (
                            <div>
                                <h1>{info.id}</h1>
                                <h1>{info.dir}</h1>
                                <h1>{info.revision}</h1>
                            </div>
                        )}
                    </Route>
                    <Route exact path="/tasks">
                        <GetTasksTasksGet
                            resolve={(tasks) =>
                                tasks &&
                                tasks.map((task: Task) => ({
                                    data: task,
                                    children: [],
                                }))
                            }
                        >
                            {(value: any) =>
                                value && (
                                    <div>
                                        <button>Expand all</button>
                                        <button>Collapse all</button>
                                        <button>Select all</button>
                                        <button>Invert selection</button>
                                        <button>Run</button>
                                        <TreeTable
                                            value={TreeState.create(value)}
                                        >
                                            <TreeTable.Column
                                                renderCell={(row: Row) => (
                                                    <span>{row.data.name}</span>
                                                )}
                                                renderHeaderCell={() => (
                                                    <span>name</span>
                                                )}
                                            />
                                            <TreeTable.Column
                                                renderCell={(row: Row) => (
                                                    <span>{row.data.type}</span>
                                                )}
                                                renderHeaderCell={() => (
                                                    <span>type</span>
                                                )}
                                            />
                                        </TreeTable>
                                    </div>
                                )
                            }
                        </GetTasksTasksGet>
                    </Route>
                    <Route path="/tasks/:id">
                        <TaskPage />
                    </Route>
                </Switch>
            </div>
        </BrowserRouter>
    );
}

function TaskPage() {
    const { id } = useParams<{id: string}>();
    return <div>{id} run use only this peon</div>;
}
