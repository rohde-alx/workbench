FROM node:16 as builder

WORKDIR /app

COPY ./package*.json ./
RUN npm ci

COPY ./ /app/
RUN npm run build

FROM nginx:mainline-alpine

COPY --from=builder /app/build/ /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf
COPY ./nginx.conf /etc/nginx/conf.d

EXPOSE 80
